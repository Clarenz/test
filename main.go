package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	myChoice := 0
	showIntroduction()
	fmt.Scanln()

	for myChoice != 2 {
		showMainMenu()
		myChoice = enterChoice(1, 4)

		switch myChoice {
		case 1:
			mathSolver()
		case 2:
			fmt.Println("Thank you for using my program.")
			fmt.Println("Enjoy the rest of your day.")
		}
	}
}

func showIntroduction() {
	fmt.Println()
	fmt.Println("College of Information and Computing Sciences")
	fmt.Println("Saint Louis University")
	fmt.Println("Baguio City")
	fmt.Println("---------------------------------------------")
	fmt.Println()
	fmt.Println("Team 7 - Go")
	fmt.Println()
	fmt.Print("Please press a key to see Main Menu...")
}

func showMainMenu() {
	fmt.Println("Main Menu ")
	fmt.Println("------------------------------")
	fmt.Println("1. Math Routines")
	fmt.Println("2. Exit")
	fmt.Println("------------------------------")
}

func showMenu1() {
	fmt.Println("Math Routine Submenu ")
	fmt.Println("-------------------------------------------------")
	fmt.Println("1. Determine whether an integer is odd or even")
	fmt.Println("2. Determine sum of a series")
	fmt.Println("3. Determine the area of a triangle")
	fmt.Println("4. Generate a Fibonacci Sequence")
	fmt.Println("5. Generate a Pascal's Triangle")
	fmt.Println("6. Back to Main Menu")
	fmt.Println("-------------------------------------------------")
}

func enterChoice(min, max int) int {
	choice := 0

	for choice < min || choice > max {
		fmt.Print("Input the number corresponding to your choice: ")
		fmt.Scanln(&choice)

		if choice < min || choice > max {
			fmt.Printf("Invalid choice. Please ensure that you enter a number from %d to %d.\n", min, max)
		}
	}

	return choice
}

func mathSolver() {
	choice := 0

	for choice != 6 {
		showMenu1()
		choice = enterChoice(1, 6)

		switch choice {
		case 1:
			var y int
			fmt.Print("Enter an integer: ")
			fmt.Scanln(&y)
			fmt.Println(detOddEven(y))
			fmt.Println()
			fmt.Print("Press enter to continue...")
			fmt.Scanln()
		case 2:
			var z int
			fmt.Print("Enter an integer: ")
			fmt.Scanln(&z)
			fmt.Printf("The sum of the series is %.2f\n", getSumOfSeries(z))
			fmt.Println()
			fmt.Print("Press enter to continue...")
			fmt.Scanln()

		case 3:
			var base, height float64
			fmt.Print("Enter the base of the triangle: ")
			fmt.Scanln(&base)
			fmt.Print("Enter the height of the triangle: ")
			fmt.Scanln(&height)
			areaOfTriangle := calculateAreaOfTriangle(base, height)
			fmt.Printf("The Area of the Triangle is: %.2f\n", areaOfTriangle)
			fmt.Println()
			fmt.Print("Press enter to continue...")
			fmt.Scanln()

		case 4:
			var t int
			fmt.Print("How many terms do you want? ")
			fmt.Scanln(&t)

			generateFibonacciSequence(t, os.Stdout)

			fmt.Println()
			fmt.Print("Press enter to continue...")
			fmt.Scanln()
		case 5:
			var nRow int
			fmt.Print("Enter number of rows for the Pascal Triangle: ")
			fmt.Scanln(&nRow)
			triangle := generatePascalsTriangle(nRow)
			printPascalsTriangle(triangle)
			fmt.Print("Press enter to continue...")
			fmt.Scanln()
		case 6:
			showMainMenu()
		}
	}
}

func detOddEven(number int) string {
	if number%2 != 0 {
		return "The number is odd."
	}
	return "The number is even."
}

func getSumOfSeries(n int) float64 {
	sum := 0.0
	if n > 1 && n%2 == 0 {
		for ctr := 2; ctr <= n; ctr += 2 {
			sum += 1.0 / float64(ctr)
		}
	}
	return sum
}

func generateFibonacciSequence(nTerms int, w io.Writer) {
	first, second := 0, 1
	var nextTerm int

	for i := 0; i < nTerms; i++ {
		if i == 0 {
			fmt.Fprintf(w, "Terms: %d", first)
			continue
		}
		if i == 1 {
			fmt.Fprintf(w, ", %d", second)
			continue
		}
		nextTerm = first + second
		first = second
		second = nextTerm
		fmt.Fprintf(w, ", %d", nextTerm)
	}
	fmt.Fprintln(w) // Print a newline at the end of the sequence.
}

func calculateAreaOfTriangle(base, height float64) float64 {
	return (base * height) / 2
}

// generatePascalsTriangle generates Pascal's Triangle up to the given number of rows.
func generatePascalsTriangle(nRows int) [][]int {
	triangle := make([][]int, nRows)
	for i := range triangle {
		triangle[i] = make([]int, i+1)
		triangle[i][0] = 1
		triangle[i][i] = 1
		for j := 1; j < i; j++ {
			// bug is placed here (it should be triangle[i-1])
			triangle[i][j] = triangle[i-0][j-1] + triangle[i-1][j]
		}
	}
	return triangle
}

// printPascalsTriangle prints Pascal's Triangle.
func printPascalsTriangle(triangle [][]int) {
	nRows := len(triangle)
	for i := range triangle {
		for space := 0; space < nRows-i; space++ {
			fmt.Print(" ")
		}
		for j := range triangle[i] {
			fmt.Printf("%d ", triangle[i][j])
		}
		fmt.Println()
	}
}
