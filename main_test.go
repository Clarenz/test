package main

import (
	"testing"
)

func TestDetOddEven(t *testing.T) {
	testCases := []struct {
		name     string
		input    int
		expected string
	}{
		{"EvenNumber", 2, "The number is even."},
		{"OddNumber", 3, "The number is odd."},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			actual := detOddEven(testCase.input)
			if actual != testCase.expected {
				t.Errorf("detOddEven(%d) = %s; want %s", testCase.input, actual, testCase.expected)
			}
		})
	}
}
